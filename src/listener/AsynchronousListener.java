package listener;

import java.io.IOException;

import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AsynchronousListener implements AsyncListener{

	@Override
	public void onComplete(AsyncEvent arg0) throws IOException {
		System.out.println("AsynchronousListener.onComplete()");
	}

	@Override
	public void onError(AsyncEvent arg0) throws IOException {
		System.out.println("AsynchronousListener.onError()");
	}

	@Override
	public void onStartAsync(AsyncEvent arg0) throws IOException {
		System.out.println("AsynchronousListener.onStartAsync()");
	}

	@Override
	public void onTimeout(AsyncEvent arg0) throws IOException {
		System.out.println("AsynchronousListener.onTimeout()");
	}

}
