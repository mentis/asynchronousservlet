package clientTestREST;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class RequestSender {

	public RequestSender() {
		super();
	}

	public static void synchronousLongProcess() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		String baseUrl = "http://localhost:8080/asynchronousWorkerServlet/";
		HttpGet getRequest = new HttpGet(baseUrl + "synchronouslongprocess");
		String result = "";
		String line = "";
		try {
			HttpResponse response = httpClient.execute(getRequest);
			System.out.println("Http response: " + response);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((line = rd.readLine()) != null) {
				result = result + line;
			}
			System.out.println(result);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void asynchronousLongProcess() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		String baseUrl = "http://localhost:8080/asynchronousWorkerServlet/";
		HttpGet getRequest = new HttpGet(baseUrl + "asynchronouslongprocess");
		String result = "";
		String line = "";
		try {
			HttpResponse response = httpClient.execute(getRequest);
			System.out.println("Http response: " + response);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((line = rd.readLine()) != null) {
				result = result + line;
			}
			System.out.println(result);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		RequestSender.asynchronousLongProcess();
		RequestSender.asynchronousLongProcess();
	}
}
