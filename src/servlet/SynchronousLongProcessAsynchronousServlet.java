package servlet;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import listener.AsynchronousListener;
import worker.SynchronousLongWoker;

@WebServlet(name = "synchronouslongprocess", urlPatterns = { "/synchronouslongprocess" }, asyncSupported = true)
public class SynchronousLongProcessAsynchronousServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("SynchronousLongProcessAsynchronousServlet.doGet()");
		req.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);

		AsyncContext asyncContext = req.startAsync(req, resp);
		asyncContext.addListener(new AsynchronousListener());

		ThreadPoolExecutor executor = (ThreadPoolExecutor) req.getServletContext().getAttribute("executor");
		executor.execute(new SynchronousLongWoker(asyncContext));
	}
}
