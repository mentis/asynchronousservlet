package servlet;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import worker.AsynchronousLongWoker;

@WebServlet(name = "asynchronouslongprocess", urlPatterns = { "/asynchronouslongprocess" })
public class AsynchronousLongProcessAsynchronousServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("AsynchronousLongProcessAsynchronousServlet.doGet()");
		ThreadPoolExecutor executor = (ThreadPoolExecutor) req.getServletContext().getAttribute("executor");
		executor.execute(new AsynchronousLongWoker());
	}
}
