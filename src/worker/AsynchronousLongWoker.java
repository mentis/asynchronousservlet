package worker;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;

public class AsynchronousLongWoker implements Runnable {

	public AsynchronousLongWoker() {
	}

	@Override
	public void run() {
		System.out.println("\nAsynchronousLongWoker.run()");
		// wait 10sec
		try {
			Thread.sleep(1000 * 10);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// do a work (write in a file)
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:/mood movie/resultAsyncServlet.txt", true), "utf-8"));
			writer.append("Asynchronous Long worker finished at: " + Calendar.getInstance().getTime().toString());
			writer.newLine();
		} catch (IOException ex) {
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
			}
		}
	}

}
