package worker;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Calendar;

import javax.servlet.AsyncContext;

public class SynchronousLongWoker implements Runnable {
    
	private AsyncContext asyncContext;

    public SynchronousLongWoker(AsyncContext asyncCtx) {
        this.asyncContext = asyncCtx;
    }

	@Override
	public void run() {
		System.out.println("SynchronousLongWoker.run()");
        // wait 10sec
        try {
			Thread.sleep(1000 * 10);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
        
        // do a work (write in a file)
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:/mood movie/resultAsyncServlet.txt", false), "utf-8"));
			writer.write("Synchronous long worker finished at: " + Calendar.getInstance().getTime().toString());
			writer.newLine();
		} catch (IOException ex) {
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
			}
		}
        
        // write the http response
        try {
            PrintWriter out = asyncContext.getResponse().getWriter();
            out.write("Processing done for longWorker");
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        //complete the processing
        asyncContext.complete();
	}

}
