package handler;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

public class BlockUntillFreeThreadHandler implements RejectedExecutionHandler {

	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor ex) {
		System.out.println("BlockUntillFreeThreadHandler.rejectedExecution()");
		if (ex.isTerminated() || ex.isShutdown()) {
			return;
		}

		boolean submitted = false;
		while (!submitted) {
			// wait for the queue to dequeue
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// retry to launch the runnable
			ex.execute(r);
			submitted = true;
		}
	}

}
